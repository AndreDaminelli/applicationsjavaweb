package aplicacao.workshop.other;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author André Nunes Daminelli
 */

public class JPA {
    public static EntityManagerFactory emf;

    public static EntityManager getEM() {
        if(emf == null) {
            emf = Persistence.createEntityManagerFactory("WorkShopJPA");
        }

        return emf.createEntityManager();
    }
}
