package aplicacao.workshop.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * @author André Nunes Daminelli
 */

@FacesValidator(value = "cpfValidator")
public class CpfValidator implements Validator {
    @Override
    public void validate(FacesContext fc, UIComponent uic, Object object) throws ValidatorException {
        if(!validCpf(String.valueOf(object))) {
            FacesMessage message = new FacesMessage();

            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("CPF inválido.");

            throw new ValidatorException(message);
        }
    }

    private boolean validCpf(String cpf) {
        if(cpf.equals("") || cpf.length() == 0) return true;
        if(cpf == null || cpf.length() != 11) return false;
        if(this.isCpfPadrao(cpf)) return true;

        try{
            Long.parseLong(cpf);
          } catch (NumberFormatException e) {
           return false;
          }

        if(this.digitoVerificadorValido(cpf)) return false;

        return true;
    }
    
    private boolean isCpfPadrao(String cpf) {
        if(cpf.equals("00000000000") || cpf.equals("11111111111") || cpf.equals("22222222222") ||
           cpf.equals("33333333333") || cpf.equals("44444444444") || cpf.equals("55555555555") ||
           cpf.equals("66666666666") || cpf.equals("77777777777") || cpf.equals("88888888888") ||
           cpf.equals("99999999999")) {
            return true;
        }

        return false;
    }

    private boolean digitoVerificadorValido(String cpf) {
        String digitoVerificador;

        digitoVerificador = calculoDigitoVerificador(cpf.substring(0, 9));

        return digitoVerificador.equals(cpf.substring(9, 11));
    }

    private String calculoDigitoVerificador(String cpf) {
        String analiseCpf;
        String digitoVerificador;
        Integer valorPeso;
        Integer somaTotal;
        Integer segundoDigito;
        Integer primeiroDigito;

        somaTotal  = 0;
        valorPeso  = 10;
        analiseCpf = cpf.substring(0, 9);

        for(int repeticao = 0; repeticao < analiseCpf.length(); repeticao++) {
            somaTotal += Integer.parseInt(analiseCpf.substring(repeticao, repeticao + 1)) * valorPeso--;
        }

        if(somaTotal % 11 == 0 || somaTotal % 11 == 1) {
            primeiroDigito = new Integer(0);
        }else{
            primeiroDigito = new Integer(11 - (somaTotal % 11));
        }

        somaTotal = 0;
        valorPeso = 11;

        for(int repeticao = 0; repeticao < analiseCpf.length(); repeticao++) {
            somaTotal += Integer.parseInt(analiseCpf.substring(repeticao, repeticao + 1)) * valorPeso--;
        }

        somaTotal += primeiroDigito * 2;

        if(somaTotal % 11 == 0 | somaTotal % 11 == 1) {
            segundoDigito = new Integer(0);
        }else{
            segundoDigito = new Integer(11 - (somaTotal % 11));
        }

        digitoVerificador = primeiroDigito.toString() + segundoDigito.toString();

        return digitoVerificador;
    }
}