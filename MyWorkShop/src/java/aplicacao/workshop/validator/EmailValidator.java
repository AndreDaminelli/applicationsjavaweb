package aplicacao.workshop.validator;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * @author André Nunes Daminelli
 */

@FacesValidator("emailValidator")
public class EmailValidator implements Validator {
    public void validate(FacesContext arg0, UIComponent arg1, Object value) throws ValidatorException {
        if(!validaEmail(String.valueOf(value))) {
            FacesMessage message = new FacesMessage();

            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Email inválido.");

            throw new ValidatorException(message);
          }
    }

    private boolean validaEmail(String email) {
        int posicaoPonto;
        int posicaoArroba;

        if(email.length() == 0) {
            return true;
        }

        if(email.length() < 10) {
            return false;
        }

        posicaoPonto  = email.indexOf(".");
        posicaoArroba = email.indexOf("@");

        if(posicaoArroba == 0 || email.indexOf("@", posicaoArroba + 1) > 0) {
            return false;
        }

        if((email.substring(1, posicaoArroba)).indexOf("@") > 0) {
            return false;
        }

        if(posicaoPonto == 0 || (posicaoPonto - posicaoArroba) < 3) {
            return false;
        }

        if((email.substring(posicaoPonto, email.length())).length() < 2) {
            return false;
        }

        return true;
    }
}