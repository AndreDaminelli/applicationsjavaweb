package aplicacao.workshop.controller;

import aplicacao.workshop.modelo.Cliente;
import aplicacao.workshop.modelo.OrdemServico;
import aplicacao.workshop.modelo.OrdemServicoItem;
import aplicacao.workshop.modelo.Produto;
import aplicacao.workshop.repositorio.ClienteRepositorio;
import aplicacao.workshop.repositorio.OrdemServicoItemRepositorio;
import aplicacao.workshop.repositorio.OrdemServicoRepositorio;
import aplicacao.workshop.repositorio.ProdutoRepositorio;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;

/**
 * @author André Nunes Daminelli
 */

@ManagedBean
@ViewScoped
public class OrdemServicoController {
    private Cliente cliente;
    private Produto produto;
    private OrdemServico ordemServico;
    private List<Produto> listaProduto = new ArrayList<Produto>();
    private List<OrdemServico> listaOrdemServico = new ArrayList<OrdemServico>();

    public OrdemServicoController() {
        this.novo();
    }

    public void adicionaProduto() {
        if(produto.getCodProduto() == null) return;

        this.listaProduto.add(this.produto);
        this.calculaTotal();
    }

    public void carregaCliente(ValueChangeEvent value) {
        Integer codigoCliente;

        if(value.getNewValue().toString().equals("")) return;

        codigoCliente = Integer.valueOf(value.getNewValue().toString());

        if(codigoCliente == null) return;

        this.cliente = ClienteRepositorio.getCliente(Integer.valueOf(value.getNewValue().toString()));
    }

    public void carregaProduto(ValueChangeEvent value) {
        Integer codigoProduto;

        if(value.getNewValue().toString().equals("")) return;

        codigoProduto = Integer.valueOf(value.getNewValue().toString());

        if(codigoProduto == null) return;

        this.produto = ProdutoRepositorio.getProduto(codigoProduto);
    }

    public void removeProduto(Produto produto) {
        this.listaProduto.remove(produto);

        this.calculaTotal();
    }

    public void calculaTotal() {
        Double valorTotal;
        Double valorMaoObra;
        Double valorUnitario;

        valorTotal    = 0.00;
        valorMaoObra  = this.ordemServico.getValorMaoObra();
        valorUnitario = 0.00;

        if(valorMaoObra == null) valorMaoObra = 0.00;

        for(Produto produtoAux: this.listaProduto) {
            valorUnitario = produtoAux.getValor();

            if(valorUnitario == null) valorUnitario = 0.00;

            valorTotal += valorUnitario;
        }

        valorTotal += valorMaoObra;

        this.ordemServico.setValorTotal(valorTotal);
    }

    public void novo() {
        this.cliente      = new Cliente();
        this.produto      = new Produto();
        this.ordemServico = new OrdemServico();
        this.listaProduto = new ArrayList<Produto>();

        this.ordemServico.setSituacao("AB");
        this.calculaTotal();
    }

    public void salvar() {
        Integer codigoOrdemServicoItem;

        List<OrdemServicoItem> ordemServicoItem = new ArrayList<OrdemServicoItem>();

        this.ordemServico.setCodOrdemServico(OrdemServicoRepositorio.getSequencialOrdemServico());
        this.ordemServico.setCliente(this.cliente);

        codigoOrdemServicoItem = OrdemServicoItemRepositorio.getSequencialOrdemServicoItem();

        for(Produto produtoAux: this.listaProduto) {
            codigoOrdemServicoItem++;

            OrdemServicoItem itemAux = new OrdemServicoItem();

            itemAux.setProduto(produtoAux);
            itemAux.setCodOrdemServicoItem(codigoOrdemServicoItem);
            itemAux.setOrdemServico(ordemServico);
            ordemServicoItem.add(itemAux);
        }

        this.ordemServico.setListaItens(ordemServicoItem);

        OrdemServicoRepositorio.salvar(this.ordemServico);

        this.novo();
    }

    public void processaOrdemServico(OrdemServico ordemServico) {
        ordemServico.setSituacao("FC");

        OrdemServicoRepositorio.salvar(ordemServico);

        this.carregaOrdemServicosAberto();
    }

    public void estornaOrdemServico(OrdemServico ordemServico) {
        ordemServico.setSituacao("AB");

        OrdemServicoRepositorio.salvar(ordemServico);

        this.carregaOrdemServicosAberto();
    }

    public void cancelaOrdemServico(OrdemServico ordemServico) {
        ordemServico.setSituacao("CN");

        OrdemServicoRepositorio.salvar(ordemServico);

        this.carregaOrdemServicosAberto();
    }

    public void carregaOrdemServicosAberto() {
        this.listaOrdemServico = OrdemServicoRepositorio.getOrdemServicosAberta();
    }

    public void carregaOrdemServicosFechado() {
        this.listaOrdemServico = OrdemServicoRepositorio.getOrdemServicosFechada();
    }

    public void carregaOrdemServicoFechadoCancelado() {
        this.listaOrdemServico = OrdemServicoRepositorio.getOrdemServicosFechadaCancelada();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public OrdemServico getOrdemServico() {
        return ordemServico;
    }

    public void setOrdemServico(OrdemServico ordemServico) {
        this.ordemServico = ordemServico;
    }

    public List<Produto> getListaProduto() {
        return listaProduto;
    }

    public void setListaProduto(List<Produto> listaProduto) {
        this.listaProduto = listaProduto;
    }

    public List<OrdemServico> getListaOrdemServico() {
        return listaOrdemServico;
    }

    public void setListaOrdemServico(List<OrdemServico> listaOrdemServico) {
        this.listaOrdemServico = listaOrdemServico;
    }
}