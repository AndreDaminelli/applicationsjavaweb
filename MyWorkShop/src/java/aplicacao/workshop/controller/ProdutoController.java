package aplicacao.workshop.controller;

import aplicacao.workshop.modelo.Produto;
import aplicacao.workshop.repositorio.ProdutoRepositorio;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author André Nunes Daminelli
 */

@ManagedBean
@ViewScoped
public class ProdutoController {
    private Produto produto;
    private List<Produto> listaProduto = new ArrayList<Produto>();

    public ProdutoController() {
        novo();
        carregaProdutos();
    }

    public void novo() {
        this.produto = new Produto();
    }

    public void carregaProdutos() {
        this.listaProduto = ProdutoRepositorio.getProdutos();
    }

    public void salvar() {
        ProdutoRepositorio.salvar(this.produto);

        novo();
        carregaProdutos();
    }

    public void excluirProduto(Produto produto) {
        ProdutoRepositorio.excluir(produto);

        novo();
        carregaProdutos();
    }

    public void excluirProduto() {
        this.excluirProduto(this.produto);
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public List<Produto> getListaProduto() {
        return listaProduto;
    }

    public void setListaProduto(List<Produto> listaProduto) {
        this.listaProduto = listaProduto;
    }
}