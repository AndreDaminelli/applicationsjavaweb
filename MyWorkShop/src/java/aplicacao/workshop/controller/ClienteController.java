package aplicacao.workshop.controller;

import aplicacao.workshop.modelo.Cliente;
import aplicacao.workshop.repositorio.ClienteRepositorio;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author André Nunes Daminelli
 */

@ManagedBean
@ViewScoped
public class ClienteController {
    private Cliente cliente;
    private List<Cliente> listaCliente = new ArrayList<Cliente>();

    public void novo() {
        this.cliente = new Cliente();
    }

    public void carregaClientes() {
        this.listaCliente = ClienteRepositorio.getClientes();
    }

    public void salvar() {
        ClienteRepositorio.salvar(this.cliente);

        novo();
        carregaClientes();
    }

    public ClienteController() {
        novo();
        carregaClientes();
    }

    public void excluirCliente(Cliente cliente) {
        ClienteRepositorio.excluir(cliente);

        novo();
        carregaClientes();
    }

    public void excluirCliente() {
        this.excluirCliente(this.cliente);
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Cliente> getListaCliente() {
        return listaCliente;
    }

    public void setListaCliente(List<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }
    
}