package aplicacao.workshop.converter;

import java.text.ParseException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.swing.text.MaskFormatter;

/**
 * @author André Nunes Daminelli
 */

@FacesConverter(value = "foneConverter")
public class foneConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String fone) {
        MaskFormatter maskFone;

        try {
            maskFone = new MaskFormatter("(##) ####-####");

            maskFone.setValueContainsLiteralCharacters(true);

            return maskFone.valueToString(fone);
        } catch (ParseException ex) {
            return fone;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object fone) {
        MaskFormatter maskFone;

        try {
            maskFone = new MaskFormatter("(##) ####-####");

            maskFone.setValueContainsLiteralCharacters(true);

            return maskFone.valueToString(fone);
        } catch (ParseException ex) {
            return (String) fone;
        }
    }
    
}
