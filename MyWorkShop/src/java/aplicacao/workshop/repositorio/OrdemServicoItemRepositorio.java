package aplicacao.workshop.repositorio;

import aplicacao.workshop.other.JPA;
import javax.persistence.EntityManager;

/**
 * @author Andre Nunes Daminelli
 */

public class OrdemServicoItemRepositorio {
    public static Integer getSequencialOrdemServicoItem() {
        EntityManager entityManager = JPA.getEM();

        Integer sequencial = entityManager.createQuery("select coalesce(max(ordemservicoitem.codOrdemServicoItem), 0) + 1" +
                                                       "  from OrdemServicoItem ordemservicoitem", Integer.class).getSingleResult();

        return sequencial;
    }
}
