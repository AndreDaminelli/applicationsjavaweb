package aplicacao.workshop.repositorio;

import aplicacao.workshop.modelo.OrdemServico;
import aplicacao.workshop.modelo.Produto;
import aplicacao.workshop.other.JPA;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

/**
 * @author André Nunes Daminelli
 */

public class OrdemServicoRepositorio {
    public static void salvar(OrdemServico ordemServico) {
        EntityManager entityManager         = JPA.getEM();
        EntityTransaction entityTransaction = entityManager.getTransaction();

        entityTransaction.begin();
        entityManager.merge(ordemServico);
        entityTransaction.commit();
    }

    public static void excluir(OrdemServico ordemServico) {
        EntityManager entityManager         = JPA.getEM();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        
        entityTransaction.begin();
        entityManager.remove(entityManager.find(OrdemServico.class, ordemServico.getCodOrdemServico()));
        entityTransaction.commit();
    }

    public static OrdemServico getOrdemServico(Integer codOrdemServico) {
        EntityManager entityManager = JPA.getEM();

        return entityManager.find(OrdemServico.class, codOrdemServico);
    }

    public static List<OrdemServico> getOrdemServicosAberta() {
        EntityManager entityManager = JPA.getEM();

        TypedQuery<OrdemServico> typedQuery = entityManager.createQuery("select ordemservico" +
                                                                        "  from OrdemServico ordemservico" +
                                                                        " where ordemservico.situacao = :situacao", OrdemServico.class);

        typedQuery.setParameter("situacao", "AB");

        return typedQuery.getResultList();
    }

    public static List<OrdemServico> getOrdemServicosFechada() {
        EntityManager entityManager = JPA.getEM();

        TypedQuery<OrdemServico> typedQuery = entityManager.createQuery("select ordemservico" +
                                                                        "  from OrdemServico ordemservico" +
                                                                        " where ordemservico.situacao = :situacao", OrdemServico.class);

        typedQuery.setParameter("situacao", "FC");

        return typedQuery.getResultList();
    }

    public static List<OrdemServico> getOrdemServicosFechadaCancelada() {
        EntityManager entityManager = JPA.getEM();

        TypedQuery<OrdemServico> typedQuery = entityManager.createQuery("select ordemservico" +
                                                                        "  from OrdemServico ordemservico" +
                                                                        " where ordemservico.situacao <> :situacao", OrdemServico.class);

        typedQuery.setParameter("situacao", "AB");

        return typedQuery.getResultList();
    }

    public static Integer getSequencialOrdemServico() {
        EntityManager entityManager = JPA.getEM();

        Integer sequencial = entityManager.createQuery("select coalesce(max(ordemservico.codOrdemServico), 0) + 1" +
                                                            "  from OrdemServico ordemservico", Integer.class).getSingleResult();

        return sequencial;
    }

    public static List<Produto> getProdutoOrdemServico() {
        EntityManager entityManager = JPA.getEM();

        TypedQuery<Produto> typedQuery = entityManager.createQuery("select ordemservico" +
                                                                        "  from OrdemServico ordemservico" +
                                                                        " where ordemservico.situacao :situacao", Produto.class);
        typedQuery.setParameter("situacao", "FC");

        return typedQuery.getResultList();
    }
}