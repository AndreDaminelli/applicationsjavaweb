package aplicacao.workshop.repositorio;

import aplicacao.workshop.modelo.Cliente;
import aplicacao.workshop.other.JPA;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

/**
 * @author André Nunes Daminelli
 */

public class ClienteRepositorio {
    public static void salvar(Cliente cliente) {
        EntityManager em    = JPA.getEM();
        EntityTransaction t = em.getTransaction();

        t.begin();
        em.merge(cliente);
        t.commit();
    }

    public static void excluir(Cliente cliente) {
        EntityManager em    = JPA.getEM();
        EntityTransaction t = em.getTransaction();

        t.begin();
        em.remove(em.find(Cliente.class, cliente.getCodCliente()));
        t.commit();
    }

    public static Cliente getCliente(Integer codigo) {
        EntityManager em = JPA.getEM();

        return em.find(Cliente.class, codigo);
    }

    public static List<Cliente> getClientes() {
        EntityManager em = JPA.getEM();

        return em.createQuery("select cli from Cliente cli", Cliente.class).getResultList();
    }

    public static List<Cliente> getClientes(String nome) {
        EntityManager em = JPA.getEM();

        TypedQuery<Cliente> query = em.createQuery("select cli from Cliente cli where cli.nome "
                                                   + "like :nome", Cliente.class);
        query.setParameter("valor", "%" + nome + "%");

        return query.getResultList();
    }
}