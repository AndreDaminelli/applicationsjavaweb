package aplicacao.workshop.repositorio;

import aplicacao.workshop.modelo.Produto;
import aplicacao.workshop.other.JPA;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

/**
 * @author André Nunes Daminelli
 */

public class ProdutoRepositorio {
    public static void salvar(Produto produto) {
        EntityManager em    = JPA.getEM();
        EntityTransaction t = em.getTransaction();

        t.begin();
        em.merge(produto);
        t.commit();
    }

    public static void excluir(Produto produto) {
        EntityManager em    = JPA.getEM();
        EntityTransaction t = em.getTransaction();

        t.begin();
        em.remove(em.find(Produto.class, produto.getCodProduto()));
        t.commit();
    }

    public static Produto getProduto(Integer codigo) {
        EntityManager em = JPA.getEM();

        return em.find(Produto.class, codigo);
    }

    public static List<Produto> getProdutos() {
        EntityManager em = JPA.getEM();

        return em.createQuery("select prod from Produto prod", Produto.class).getResultList();
    }

    public static List<Produto> getProdutos(String descricao) {
        EntityManager em = JPA.getEM();

        TypedQuery<Produto> query = em.createQuery("select prod from Produto prod where prod.descricao "
                                                   + "like :descricao", Produto.class);
        query.setParameter("valor", "%" + descricao + "%");

        return query.getResultList();
    }
}