package aplicacao.workshop.modelo;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * @author André
 */

@Entity
public class OrdemServicoItem {
    @Id
    private Integer codOrdemServicoItem;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "codProduto")
    private Produto produto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="codOrdemServico")
    private OrdemServico ordemServico;

    public Integer getCodOrdemServicoItem() {
        return codOrdemServicoItem;
    }

    public void setCodOrdemServicoItem(Integer codOrdemServicoItem) {
        this.codOrdemServicoItem = codOrdemServicoItem;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public OrdemServico getOrdemServico() {
        return ordemServico;
    }

    public void setOrdemServico(OrdemServico ordemServico) {
        this.ordemServico = ordemServico;
    }
}