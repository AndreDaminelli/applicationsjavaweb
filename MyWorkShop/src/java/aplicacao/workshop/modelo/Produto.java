package aplicacao.workshop.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author André Nunes Daminelli
 */

@Entity
public class Produto {
    @Id
    private Integer codProduto;
    private String descricao;
    private Double valor;

    public Integer getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(Integer codProduto) {
        this.codProduto = codProduto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}