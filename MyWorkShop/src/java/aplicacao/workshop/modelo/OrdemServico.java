package aplicacao.workshop.modelo;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * @author André Nunes Daminelli
 */

@Entity
public class OrdemServico {
    @Id
    private Integer codOrdemServico;
    private String descricao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "codCliente")
    private Cliente cliente;

    @OneToMany(fetch = FetchType.LAZY,
               mappedBy = "OrdemServico",
               orphanRemoval = true,
               cascade = CascadeType.ALL)
    private List<OrdemServicoItem> listaItens = new ArrayList<OrdemServicoItem>();

    private Double valorMaoObra;
    private Double valorTotal;
    private String situacao;

    public Integer getCodOrdemServico() {
        return codOrdemServico;
    }

    public void setCodOrdemServico(Integer codOrdemServico) {
        this.codOrdemServico = codOrdemServico;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<OrdemServicoItem> getListaItens() {
        return listaItens;
    }

    public void setListaItens(List<OrdemServicoItem> listaItens) {
        this.listaItens = listaItens;
    }
    public Double getValorMaoObra() {
        return valorMaoObra;
    }

    public void setValorMaoObra(Double valorMaoObra) {
        this.valorMaoObra = valorMaoObra;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }
}